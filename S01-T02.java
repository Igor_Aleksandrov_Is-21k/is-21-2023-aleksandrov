import java.util.Scanner;
public class S01-T02 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите количество секунд: ");
        int count = s.nextInt();
        int hours = count / 3600 % 24;
        int minutes = (count / 60) % 60;
        int seconds = count % 60;
        System.out.print(hours/10 + hours % 10 + ":" + minutes/10 + minutes % 10 + ":" + seconds/10 + seconds%10);

    }
}